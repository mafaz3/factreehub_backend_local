const express = require('express');
const router = express.Router();

const feedbackController = require('../controller/feedback.controller');

router.post("/adminServiceProviderFeedback", feedbackController.adminServiceProviderFeedback);
router.post("/adminCustomerFeedback", feedbackController.adminCustomerFeedback);
router.get("/getAdminServiceProviderFeedback", feedbackController.getAdminServiceProviderFeedback);
router.get("/getAdminCustomerFeedback", feedbackController.getAdminCustomerFeedback);
router.get("/getAllFeedback", feedbackController.getAllFeedback);

module.exports = router;