const express = require('express');
const router = express.Router();

const chatController = require('../controller/chat.controller')

router.post("/sendMail", chatController.sendChat);

module.exports = router;