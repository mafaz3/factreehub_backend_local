const express = require('express');
const router = express.Router();

const authController = require('../controller/auth.controller')

router.post("/signUpAdmin", authController.signUpAdmin);
router.post("/signUpServiceProvider", authController.signUpServiceProvider);
router.post("/signUpCustomer", authController.signUpCustomer)

module.exports = router;