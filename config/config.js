const mongoose = require('mongoose');

const mongooseOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // Add retry logic
    serverSelectionTimeoutMS: 5000, // Timeout for server selection
    socketTimeoutMS: 45000, // Timeout for individual operations
};

mongoose.connect('mongodb+srv://mafaz_mafs:yoursdad@cluster0.h4l1eim.mongodb.net/your-database-name', mongooseOptions)
    .catch(err => {
        console.error('MongoDB connection error:', err);
    });

const db = mongoose.connection;

db.on('error', (error) => {
    console.error('MongoDB connection error:', error);
});

db.once('open', () => {
    console.log('Connected to MongoDB');
});

module.exports = { mongoose, db };
