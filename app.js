const express = require('express');
const app = express();

const chatRoutes = require('./routes/chat.route');
const authRoutes = require('./routes/auth.route');
const feedbackRoutes = require('./routes/feedback.route');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', '*');
    res.append('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    res.append('Access-Control-Allow-Headers', '*');
    next();
});

app.listen(4000);
app.use('/api', chatRoutes);
app.use('/auth', authRoutes);
app.use('/feedback', feedbackRoutes);