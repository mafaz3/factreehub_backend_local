const bcrypt = require("bcryptjs");

class Common {
    async hashPassword(password, saltRounds) {
        return new Promise((resolve, reject) => {
            bcrypt.hash(password, saltRounds, (err, hash) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(hash);
                }
            });
        });
    }
}

module.exports = Common;