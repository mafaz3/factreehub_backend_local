const service = require('../service/feedback.service');

exports.adminServiceProviderFeedback = async (req, res) => {
    try {
        const responseFeedback = await service.adminServiceProviderFeedback(req.body);
        res.status(200).json(responseFeedback);
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
};

exports.adminCustomerFeedback = async (req, res) => {
    try {
        const responseFeedback = await service.adminCustomerFeedback(req.body);
        res.status(200).json(responseFeedback);
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
};

exports.getAdminServiceProviderFeedback = async (req, res) => {
    try {
        const responseFeedback = await service.getAdminServiceProviderFeedback();
        res.status(200).json(responseFeedback);
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
};

exports.getAdminCustomerFeedback = async (req, res) => {
    try {
        const responseFeedback = await service.getAdminCustomerFeedback();
        res.status(200).json(responseFeedback);
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
};

exports.getAllFeedback = async (req, res) => {
    try {
        const responseFeedback = await service.getAllFeedback();
        res.status(200).json(responseFeedback);
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
};