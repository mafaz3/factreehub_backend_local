const service = require('../service/auth.service');

exports.signUpAdmin = async (req, res) => {
    try {
        const responseSignupAdmin = await service.signUpAdmin(req.body);
        res.status(200).send(responseSignupAdmin);
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
};

exports.signUpServiceProvider = async (req, res) => {
    try {
        const responsesignUpServiceProvider = await service.signUpServiceProvider(req.body);
        res.status(200).send(responsesignUpServiceProvider);
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
};

exports.signUpCustomer = async (req, res) => {
    try {
        const responseSignupCustomer = await service.signUpCustomer(req.body);
        res.status(200).send(responseSignupCustomer);
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
};