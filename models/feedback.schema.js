const { mongoose } = require('../config/config');

const adminServiceProviderFeedbackSchema = new mongoose.Schema({
    message: String,
    authRole: String,
    date: {
        type: Date,
        default: Date.now
    }
});
const adminCustomerFeedbackSchema = new mongoose.Schema({
    message: String,
    authRole: String,
    date: {
        type: Date,
        default: Date.now
    }
});

const adminServiceProviderFeedbackData = mongoose.model('adminServiceProviderFeedback', adminServiceProviderFeedbackSchema);
const adminCustomerFeedbackData = mongoose.model('adminCustomerFeedback', adminCustomerFeedbackSchema);

module.exports = { adminServiceProviderFeedbackData, adminCustomerFeedbackData };