const { mongoose } = require('../config/config');

const adminAuthSchema = new mongoose.Schema({
    fullName: String,
    phoneNumber: String,
    email: String,
    industry: String,
    customer: Number,
    password: String,
    authRole: String
});

const serviceProviderAuthSchema = new mongoose.Schema({
    fullName: String,
    phoneNumber: String,
    email: String,
    password: String,
    totalWorkHours: Number,
    wokingFullTime: String,
    fullTimeOrPartTime: String,
    education: String,
    educationDiscipline: String,
    linkedInId: String,
    country: String,
    serviceCapablities: String,
    softwareCapablitiesAndOwnership: String,
    bankName: String,
    accounterName: String,
    accounterNumber: String,
    ifscCode: String,
    branchName: String,
    authRole: String
});

const CustomerAuthSchema = new mongoose.Schema({
    fullName: String,
    phoneNumber: String,
    email: String,
    password: String,
    companyName: String,
    role: String,
    industrySegments: String,
    website: String,
    organizationType: String,
    organizationSize: Number,
    country: String,
    serviceRequired: String,
    softwareUsed: String,
    authRole: String
});

const UserAdminData = mongoose.model('AdminSignUp', adminAuthSchema);
const UserServiceProviderData = mongoose.model('ServiceProviderSignUp', serviceProviderAuthSchema);
const UserCustomerData = mongoose.model('CustomerSignUp', CustomerAuthSchema);

module.exports = { UserAdminData, UserServiceProviderData, UserCustomerData };