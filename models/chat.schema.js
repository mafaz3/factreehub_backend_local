const { mongoose } = require('../config/config');

const chatSchema = new mongoose.Schema({
    name: String,
    email: String,
    subject: String,
    message: String
});

const Chat = mongoose.model('Chat', chatSchema);

module.exports = Chat;