const Chat = require('../models/chat.schema');
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'mafaz@mavencart.com',
        pass: 'MafazMalik0206'
    }
});

exports.sendChat = async (chatData) => {
    const chat = new Chat(chatData);
    try {
        const doc = await chat.save();

        const mailOptions = {
            from: 'mafaz@mavencart.com',
            to: 'mafaz@mavencart.com',
            // to: `${chatData.email}`,
            subject: `${chatData.subject}`,
            text: `
                    Name: ${chatData.name}
                    Email: ${chatData.email}
                    Subject: ${chatData.subject}
                    Message: ${chatData.message}
                `
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.error(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });

        return doc;
    } catch (error) {
        throw error;
    }
}