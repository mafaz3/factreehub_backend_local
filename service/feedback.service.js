const { adminServiceProviderFeedbackData, adminCustomerFeedbackData } = require('../models/feedback.schema');
const { UserAdminData, UserServiceProviderData, UserCustomerData } = require('../models/auth.schema');

exports.adminServiceProviderFeedback = async (adminServiceProviderFeedback) => {
    try {
        const { authorEmail } = adminServiceProviderFeedback;
        console.log(`Searching for author with email: ${authorEmail}`);

        const adminAuthor = await UserAdminData.findOne({ email: authorEmail });

        if (!adminAuthor) {
            const serviceProviderAuthor = await UserServiceProviderData.findOne({ email: authorEmail });

            if (!serviceProviderAuthor) {
                console.log(`Author not found for email: ${authorEmail}`);
                throw new Error('Author not found');
            }

            const feedbackData = new adminServiceProviderFeedbackData({
                message: adminServiceProviderFeedback.message,
                authRole: serviceProviderAuthor.authRole
            });

            const savedFeedback = await feedbackData.save();
            return savedFeedback;
        }

        const feedbackData = new adminServiceProviderFeedbackData({
            message: adminServiceProviderFeedback.message,
            authRole: adminAuthor.authRole
        });

        const savedFeedback = await feedbackData.save();
        return savedFeedback;
    } catch (error) {
        console.error(error);
        throw error;
    }
};

exports.adminCustomerFeedback = async (adminCustomerFeedback) => {
    try {
        const { authorEmail } = adminCustomerFeedback;
        console.log(`Searching for author with email: ${authorEmail}`);

        const adminAuthor = await UserAdminData.findOne({ email: authorEmail });

        if (!adminAuthor) {
            const customerAuthor = await UserCustomerData.findOne({ email: authorEmail });

            if (!customerAuthor) {
                console.log(`Author not found for email: ${authorEmail}`);
                throw new Error('Author not found');
            }

            const feedbackData = new adminCustomerFeedbackData({
                message: adminCustomerFeedback.message,
                authRole: customerAuthor.authRole
            });

            const savedFeedback = await feedbackData.save();
            return savedFeedback;
        }

        const feedbackData = new adminCustomerFeedbackData({
            message: adminCustomerFeedback.message,
            authRole: adminAuthor.authRole
        });

        const savedFeedback = await feedbackData.save();
        return savedFeedback;
    } catch (error) {
        console.error(error);
        throw error;
    }
};

exports.getAdminServiceProviderFeedback = async () => {
    try {
        const feedback = await adminServiceProviderFeedbackData.find();

        const formattedFeedback = feedback.map(item => {
            const date = new Date(item.date);
            const options = { year: 'numeric', month: 'short', day: '2-digit', hour: '2-digit', minute: '2-digit' };
            const formattedDate = date.toLocaleString('en-US', options);
            return {
                message: item.message,
                authRole: item.authRole,
                date: formattedDate
            };
        });

        return formattedFeedback;
    } catch (error) {
        throw error;
    }
};

exports.getAdminCustomerFeedback = async () => {
    try {
        const feedback = await adminCustomerFeedbackData.find();

        const formattedFeedback = feedback.map(item => {
            const date = new Date(item.date);
            const options = { year: 'numeric', month: 'short', day: '2-digit', hour: '2-digit', minute: '2-digit' };
            const formattedDate = date.toLocaleString('en-US', options);
            return {
                message: item.message,
                authRole: item.authRole,
                date: formattedDate
            };
        });

        return formattedFeedback;
    } catch (error) {
        throw error;
    }
};

exports.getAllFeedback = async () => {
    try {
        const serviceProviderFeedback = await adminServiceProviderFeedbackData.find();
        const customerFeedback = await adminCustomerFeedbackData.find();

        const feedback = [...serviceProviderFeedback, ...customerFeedback];

        feedback.sort((a, b) => a.date - b.date);

        const formattedFeedback = feedback.map(item => {
            const date = new Date(item.date);
            const options = { year: 'numeric', month: 'short', day: '2-digit', hour: '2-digit', minute: '2-digit' };
            const formattedDate = date.toLocaleString('en-US', options);
            return {
                message: item.message,
                authRole: item.authRole,
                date: formattedDate
            };
        });

        return formattedFeedback;
    } catch (error) {
        throw error;
    }
};