const { UserAdminData, UserServiceProviderData, UserCustomerData } = require("../models/auth.schema");
const Common = require('../utils/common');
const common = new Common();

exports.signUpAdmin = async (userData) => {
    if (userData.password) {
        userData.password = await common.hashPassword(userData.password, 11);
    }

    const userAdminData = new UserAdminData(userData);

    try {
        const signUpResponse = await userAdminData.save();
        console.log(signUpResponse);

        return signUpResponse;
    } catch (error) {
        throw error;
    }
};

exports.signUpServiceProvider = async (userData) => {
    if (userData.password) {
        userData.password = await common.hashPassword(userData.password, 11);
    }

    const userServiceProviderData = new UserServiceProviderData(userData);

    try {
        const signUpResponse = await userServiceProviderData.save();
        console.log(signUpResponse);

        return signUpResponse;
    } catch (error) {
        throw error;
    }
};

exports.signUpCustomer = async (userData) => {
    if (userData.password) {
        userData.password = await common.hashPassword(userData.password, 11);
    }

    const userCustomerData = new UserCustomerData(userData);

    try {
        const signUpResponse = await userCustomerData.save();
        console.log(signUpResponse);

        return signUpResponse;
    } catch (error) {
        throw error;
    }
};